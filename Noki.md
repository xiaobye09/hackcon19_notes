# Noki
## Question
>I was told Vigenère Cipher is secure as long as ```length(key) == length(message).``` So I did just that!<br><br>Break this: g4iu{ocs_oaeiiamqqi_qk_moam!}e0gi


## Solution
First,we know flags follow the format <br>
 ```d4rk{**** some-text-here ***}c0de``` like this,so I tryed to find the connection between the ciphertext and the plaintext<br>
 ```g4iu{ocs_oaeiiamqqi_qk_moam!}e0gi``` .

1. I found that in the ciphertext ```i```, there are two kinds of output ```r```&```e``` through the table.  
So I tryed to figer out the connection between them,then I found we can search ciphertext's character in the table (not include first row & first column) where it can correspond same character in first row & first colum.

2. Following is the correspond of each character in ciphertext.  

**ciphertext**|o|c|s|\_|o|a|e|i|i|a|m|q|q|i|\_|q|k|\_|m|o|a|m|!
----------|-|-|-|- |-|-|-|-|-|-|-|-|-|-|- |-|-|- |-|-|-|-|-
**case1**     |u|b|j|\_|u|a|c|r|r|n|g|v|v|r|\_|v|s|\_|g|u|a|g|!
**case2**     |h|o|w|\_|h|n|p|e|e|a|t|i|i|e|\_|i|f|\_|t|h|n|t|!

3. Next, string the meaningful sentences together by arranging the different characters of **case1** and **case2**.

4. Finally,got the flag!
		```
		d4rk{how_uncreative_is_that!}c0de
		```
<center><img src="./src/Noki.PNG"></center>